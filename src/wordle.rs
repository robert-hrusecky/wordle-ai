use std::{
    collections::HashMap,
    convert::Infallible,
    io::{self, Write},
    str::{self, Utf8Error},
};

use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum CharHint {
    Correct,
    WrongSpot,
    Incorrect,
}

impl CharHint {
    fn from(s: &str) -> [Self; 5] {
        let mut out = [Self::Incorrect; 5];
        for (i, c) in s.chars().enumerate() {
            out[i] = match c {
                'G' => Self::Correct,
                'Y' => Self::WrongSpot,
                'B' => Self::Incorrect,
                _ => panic!(),
            };
        }
        out
    }
}

fn wordle(input: &[u8], target: &[u8]) -> [CharHint; 5] {
    assert!(input.len() == 5 && target.len() == 5);
    let mut out = [CharHint::Incorrect; 5];
    let mut remaining = [b'\0'; 5];
    let mut remainig_size = 0;
    for (i, (&i_char, &t_char)) in input.iter().zip(target.iter()).enumerate() {
        if i_char == t_char {
            out[i] = CharHint::Correct;
        } else {
            remaining[remainig_size] = t_char;
            remainig_size += 1;
        }
    }

    for (i, &c) in input.iter().enumerate() {
        if let CharHint::Correct = out[i] {
            continue;
        }

        if let Some(remove_index) = remaining.iter().take(remainig_size).position(|t| *t == c) {
            out[i] = CharHint::WrongSpot;
            remainig_size -= 1;
            remaining[remove_index] = remaining[remainig_size];
        }
    }
    out
}

fn wordle_fast(input: &[u8], target: &[u8]) -> u8 {
    let mut feature = 0;
    for hint in wordle(input, target) {
        feature += hint as u8;
        feature *= 3;
    }
    feature
}

pub trait Adversary {
    type Error: std::error::Error + 'static;

    fn respond(&self, guess: &[u8]) -> Result<[CharHint; 5], Self::Error>;
}

pub struct UserAdversary;

#[derive(Debug, Error)]
pub enum UserError {
    #[error("Utf8 error")]
    Utf8Error(Utf8Error),
    #[error("I/O error")]
    IoError(io::Error),
}

impl From<Utf8Error> for UserError {
    fn from(e: Utf8Error) -> Self {
        Self::Utf8Error(e)
    }
}

impl From<io::Error> for UserError {
    fn from(e: io::Error) -> Self {
        Self::IoError(e)
    }
}

impl Adversary for UserAdversary {
    type Error = UserError;

    fn respond(&self, guess: &[u8]) -> Result<[CharHint; 5], Self::Error> {
        println!("Guess: {}", str::from_utf8(guess)?);
        print!("Response: ");
        io::stdout().flush()?;
        let mut real_output = String::new();
        io::stdin().read_line(&mut real_output)?;
        Ok(CharHint::from(&real_output.trim()))
    }
}

pub struct FixedWordAdversary {
    target_word: [u8; 5],
}

impl FixedWordAdversary {
    pub fn new(target_word: &[u8]) -> Self {
        let mut result = Self {
            target_word: [0; 5],
        };
        result.target_word.copy_from_slice(target_word);
        result
    }
}

impl Adversary for FixedWordAdversary {
    type Error = Infallible;

    fn respond(&self, guess: &[u8]) -> Result<[CharHint; 5], Self::Error> {
        let response = wordle(guess, &self.target_word);
        Ok(response)
    }
}

pub struct Wordle<'a, A> {
    words: Vec<&'a [u8]>,
    working_set: Vec<&'a [u8]>,
    adversary: A,
}

pub struct WordleStat<'a> {
    pub input_set_size: usize,
    pub guess: &'a [u8],
    pub response: [CharHint; 5],
    pub score: u32,
}

impl<'a, A: Adversary> Wordle<'a, A> {
    pub fn new(words: Vec<&'a [u8]>, adversary: A) -> Self {
        Self {
            working_set: words.clone(),
            adversary,
            words,
        }
    }

    fn compute_guess(&self) -> (&'a [u8], u32) {
        self.words
            .iter()
            .map(|&word| {
                let mut output_counts = HashMap::new();
                for target_guess in &self.working_set {
                    let output = wordle_fast(word, target_guess);
                    *output_counts.entry(output).or_insert(0) += 1;
                }
                (word, output_counts.values().map(|&c| c * c).sum())
            })
            .min_by_key(|(_, score)| *score)
            .unwrap()
    }

    fn narrow_list(&mut self, guess: &[u8], response: [CharHint; 5]) {
        self.working_set
            .retain(|target_guess| wordle(guess, target_guess) == response);
    }

    pub fn run(
        &mut self,
        initial_guess: &'a [u8],
        initial_score: u32,
        initial_set_size: usize,
    ) -> Result<Vec<WordleStat<'a>>, Box<dyn std::error::Error>> {
        let mut stats = Vec::new();

        let initial_response = self.adversary.respond(initial_guess)?;

        stats.push(WordleStat {
            guess: initial_guess,
            response: initial_response,
            score: initial_score,
            input_set_size: initial_set_size,
        });

        if initial_response == CharHint::from("GGGGG") {
            return Ok(stats);
        }

        self.narrow_list(initial_guess, initial_response);


        while self.working_set.len() > 1 {
            let (guess, score) = self.compute_guess();
            let response = self.adversary.respond(guess)?;
            stats.push(WordleStat {
                guess,
                response,
                score,
                input_set_size: self.working_set.len(),
            });
            self.narrow_list(guess, response);
        }
        // TODO: Make this not panic on error
        let guess = *self
            .working_set
            .first()
            .expect("The working_set was empty...");
        let response = self.adversary.respond(guess)?;
        stats.push(WordleStat {
            guess,
            response,
            score: 1,
            input_set_size: self.working_set.len(),
        });
        assert_eq!(response, CharHint::from("GGGGG"));
        assert!(!stats.is_empty());
        Ok(stats)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_wordle() {
        assert_eq!(wordle(b"champ", b"chore"), CharHint::from("GGBBB"));
        assert_eq!(wordle(b"cheek", b"eagre"), CharHint::from("BBYYB"));
        assert_eq!(wordle(b"cheek", b"aaaea"), CharHint::from("BBBGB"));
        assert_eq!(wordle(b"cheek", b"chore"), CharHint::from("GGYBB"));
    }
}
