use std::{
    fs::File,
    io::Read,
    iter,
    sync::{atomic::AtomicU64, Arc, atomic::Ordering},
    thread,
    time::{self, Duration, Instant},
};

use wordle::{FixedWordAdversary, UserAdversary, Wordle};

use rayon::prelude::*;

mod wordle;

fn main() {
    let mut words = Vec::new();
    let _ = File::open("official.txt")
        .unwrap()
        .read_to_end(&mut words)
        .unwrap();
    let words: Vec<_> = words
        .split(|c| *c == b'\n')
        .filter(|w| !w.is_empty())
        .collect();

    let guess_hist: Vec<AtomicU64> =
        iter::repeat_with(|| AtomicU64::new(0)).take(10).collect();

    words.par_iter().for_each(|target_word| {
        //let start = Instant::now();
        let mut wordle = Wordle::new(words.clone(), FixedWordAdversary::new(target_word));
        // println!("Init time: {}", start.elapsed().as_secs_f64());
        //let start = Instant::now();
        let stats = wordle.run(b"lares", 3745512, 12972).unwrap();

        let guesses = stats.len();
        guess_hist[guesses].fetch_add(1, Ordering::Relaxed);
        // println!("Run time: {}", start.elapsed().as_secs_f64());
        // println!("Guesses: {}", stats.len());
        if guesses == 8 {
            println!("---------- {} ----------", String::from_utf8_lossy(target_word));
            for stat in stats {
                println!("Guess: {}; Response: {:?}", String::from_utf8_lossy(stat.guess), stat.response);
                println!("Score: {}; Set size: {}", stat.score, stat.input_set_size);
            }
        }
    });

    let mut sum = 0;
    for (i, word_count) in guess_hist.iter().enumerate() {
        let word_count = word_count.load(Ordering::Relaxed);
        sum += word_count;
        println!("{}: {}", i, word_count);
    }
    println!("Total {}", sum);
    // for (index, target_word) in words.par_iter().enumerate() {
    // }
    //let mut wordle = Wordle::new(words, UserAdversary);
    // println!("---------- Stats ----------");
    // for stat in stats {
    //     println!("Guess: {}; Response: {:?}", String::from_utf8_lossy(stat.guess), stat.response);
    //     println!("Score: {}; Set size: {}", stat.score, stat.input_set_size);
    // }
}
